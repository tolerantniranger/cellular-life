import pygame
from random import randint
from copy import deepcopy
import numpy as np
#from numba import njit

RES = WIDTH, HEIGHT = 1000, 900
TILE = 8 #мелкость разбиения может регулироваться
W, H = WIDTH // TILE, HEIGHT // TILE
FPS = 6 #скорость воспроизведения может регулироваться

pygame.init()
surface = pygame.display.set_mode(RES)
clock = pygame.time.Clock()

next_field = np.array([[0 for i in range(W)] for j in range(H)]) #массив исходного состояния
current_field = [[randint(0, 1) for i in range(W)] for j in range(H)] #массив текущего состояния
#current_field = np.array([[1 if not i % 117 else 0 for i in range(W)] for j in range(H)])
#current_field = np.array([[0 for i in range(W)] for j in range(H)])
#for i in range(H):
    #current_field[i][i + (W - H) // 2] - 1
    #current_field[H - i - 1][i + (W - H) // 2] - 1
#current_field = np.array([[1 if i - W // 2 or H // 2 else 0 for i in range(W)] for i in range(H)])

def check_cells(current_field, next_field):
    res = []
    for x in range(W):
        for y in range(H):
            count = 0
            for j in range(y - 1, y + 2):
                for i in range(x - 1, x + 2):
                    if current_field[i % H][j % W] == 1:
                        count += 1
                if current_field[y][x] == 1:
                    count -= 1
                    if count == 2 or count == 3:
                        next_field[y][x] = 1
                        res.append((x, y))
                    else:
                        next_field[y][x] = 0
                else:
                    if count == 3:
                        next_field[y][x] = 1
                        res.append((x, y))
                    else:
                        next_field[y][x] = 0
    return next_field, res

while True:

    surface.fill(pygame.Color('black'))
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exit()


    #мини жизнь
    next_field, res = check_cells(current_field, next_field)
    [pygame.draw.rect(surface, pygame.Color('darkorange'),
                     (x * TILE + 1, y * TILE + 1, TILE - 1, TILE - 1)) for x, y in res]


    current_field = deepcopy(next_field)

    #print(clock.get_fps())
    pygame.display.flip()
    clock.tick(FPS)

